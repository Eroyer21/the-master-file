http://jorr.cs.georgefox.edu/courses/csis430-algorithms/

<table class="table table-hover visible-xs">
			<tbody>
<tr class=""><td>
<h5>Week 1</h5>
<p>Introduction &amp; Scala</p>
<p>
<em>Reading</em>:&nbsp;Zybooks Chp. 1<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 2</h5>
<p>Running Time and Divide &amp; Conquer</p>
<p>
<em>Reading</em>:&nbsp;Zybooks Chp. 2<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 3</h5>
<p>Sorting</p>
<p>
<em>Reading</em>:&nbsp;Zybooks Chp. 3 &amp; 4<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 4</h5>
<p>Graphs &amp; Shortest Path</p>
<p>
<em>Reading</em>:&nbsp;Zybooks Chp. 5 &amp; 6 &nbsp; &nbsp; <a href="https://web.engr.oregonstate.edu/~glencora/wiki/uploads/dijkstra-proof.pdf">Dijkstra Proof</a> <br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 5</h5>
<p>MST &amp; Network Flow</p>
<p>
<em>Reading</em>:&nbsp;Zybooks Chp. 7 &amp; 8 <br><a href="https://youtu.be/daXzIYKhcnQ">Network Flow Video</a><br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 6</h5>
<p>More Greedy Algorithms</p>
<p>
<em>Reading</em>:&nbsp;Zybooks Chp. 9<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 7</h5>
<p>Introduction to <em>NP</em></p>
<p>
<em>Reading</em>:&nbsp;Zybooks Chp. 10<br>
</p>
</td></tr>
<tr class="warning"><td>
<h5>3/9</h5>
<p><b>Midterm</b></p>
<p>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 8 — 9</h5>
<p>Dynamic Programming</p>
<p>
<em>Reading</em>:&nbsp;Zybooks Chp. 11 <br> <a href="https://youtu.be/XnQG0VchZsk">0-1 Knapsack Video</a> <br> <a href="https://youtu.be/6jqlBDYNrL0">Held-Karp TSP Video</a> <br> <a href="https://youtu.be/ePwLMyR2Sy0">Held-Karp TSP "Rewind" Clarification</a><br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 10</h5>
<p>Randomized Algorithms</p>
<p>
<em>Reading</em>:&nbsp;Zybooks Chp. 12 <br> <a href="https://youtu.be/-uv5VWlfkqk">Intro. &amp; primality testing.</a> <br> <a href="https://youtu.be/te4PYYAdTJE">Genetic Algorithm for the TSP video</a> <br> <a href="papers/geneticTSP.pdf">Genetic TSP PDF</a><br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 11</h5>
<p>Backtracking Algorithms</p>
<p>
<em>Reading</em>:&nbsp;Zybooks Chp. 13 <br> <a href="https://youtu.be/_edMalQd74A">Intro. to Backtracking with N-Queens</a> <br> <a href="https://youtu.be/qK1BFAk04h0">Branch &amp; Bound</a> <br><br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 12</h5>
<p><strong>Spring Break</strong> – <em>no class</em></p>
<p>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 13</h5>
<p>Backtracking Algorithms &amp; Logic</p>
<p>
<em>Reading</em>:&nbsp;  <a href="https://www.win.tue.nl/~kbuchin/teaching/2IL15/backtracking.pdf">TSP Backtracking</a> <br> <a href="papers/sudoku.pdf">PDF</a> <br> <a href="https://youtu.be/Hjcxbop8RmY">Intro. to Logic Video</a> <br> <a href="https://youtu.be/uwuU5T25xG0">Conjunctive Normal Form (CNF) and Satisfiability (SAT) Video</a><br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 14</h5>
<p>Encryption</p>
<p>
<em>Reading</em>:&nbsp;Zybooks Chp. 14 <br> <a href="https://youtu.be/11a0LVgdy7o">Intro. to Cryptography</a> <br> <a href="papers/Kaliski.pdf">PDF</a><br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 15</h5>
<p>Special Topics</p>
<p>
<em>Reading</em>:&nbsp;<a href="https://youtu.be/GGRPaxWBvlw">CS Theory, History, and Related Philosophy Video</a><br>
</p>
</td></tr>
</tbody>
			</table>